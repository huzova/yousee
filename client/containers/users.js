import React, { Component } from 'react';
import { connect } from 'react-redux';
import UsersList from '../components/User/usersList';
import Intro from '../components/Main/intro';
import { fetchUsers } from '../redux/actions';
import Loader from '../components/Main/loader';

class Users extends Component {

    componentDidMount() {
        this.props.onFetchUsers();
        window.scrollTo(0, 0);
    }

    render (){
        return(
            <div>
                <Intro message="Here you can find all users!" displayBtn={false}/>
                {
                    this.props.users &&
                    <UsersList list={this.props.users}></UsersList>
                }
                {
                    !this.props.users &&
                    <Loader/>
                }
            </div>
        )
    }
}

const mapStatetoProps = (state) => {
    if (state){
        return {users: state.users}
    } 
    return {}

}

const mapDispatchprops = (dispatch) => {
    return {onFetchUsers:() => dispatch(fetchUsers())}
}

export default connect(mapStatetoProps, mapDispatchprops) (Users);