import React, { Component } from 'react';
import Intro from '../components/Main/intro';
import { connect } from 'react-redux';
import { fetchUser } from '../redux/actions';
import Loader from '../components/Main/loader';
import UserDetail from '../components/User/userDetail';

class UserDetails extends Component {

    componentDidMount() {
        const id = this.props.match.params.id;
        this.props.onFetchUser(id);
        window.scrollTo(0, 0);
    }

    render () {
        return (
            <div>
                <Intro message="User details" displayBtn={true}/>
                {
                    !this.props.user &&
                    <Loader/>
                }
                {
                    this.props.user &&
                    <div>
                        <UserDetail user={this.props.user} />
                    </div>
                }
            </div>
        )
    }
}

const mapStatetoProps = (state) => {
    if (state && state.user) {
        return {user: state.user}
    }
    return {}
}

const mapDispatchprops = (dispatch) => {
    return {onFetchUser:(id) => dispatch(fetchUser(id))}
}

export default connect(mapStatetoProps, mapDispatchprops) (UserDetails);