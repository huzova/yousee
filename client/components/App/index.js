import React, { Component } from 'react';
import Router from '../Main/router';
import Header from '../Main/header';
import Footer from '../Main/footer';

class App extends Component {
  
  render() {
    return (
      <div>
        <Header/>
        <main><Router/></main>
        <Footer/>
      </div>
    );
  }

}

export default App;