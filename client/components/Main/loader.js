import React from 'react';

const Loader = props => {
    return(
        <div className="loader">
            <span className="loader__line"></span>
            <span className="loader__line"></span>
            <span className="loader__line"></span>
            <span className="loader__line"></span>
        </div>
    )
}

export default Loader;