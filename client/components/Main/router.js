import React from 'react'; 
import { Switch, Route } from 'react-router-dom';
import Users from '../../containers/users';
import UserDetails from '../../containers/userDetails';

const Router = props => {
    return (
        <Switch>
            <Route exact path='/' component={Users}/>
            <Route exact path='/users/:id' component={UserDetails}/>
        </Switch>
    )
}

export default Router;