import React from 'react';
import { withScriptjs, withGoogleMap, GoogleMap, Marker } from "react-google-maps"

const MapMarker = withScriptjs(withGoogleMap((props) => 
  <GoogleMap
    defaultZoom={3}
    defaultCenter={{ lat: props.lat, lng: props.lng }}
  >
    {
      props.isMarkerShown &&
      <Marker position={{ lat: props.lat, lng: props.lng }} />
    }

  </GoogleMap>
))


export default MapMarker;