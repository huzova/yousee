import React from 'react';
import { Link } from 'react-router-dom';

const Intro = (props) => {
    return (
        <div>
            <section className="intro"> 
                {   
                    props.displayBtn &&
                    <div>
                        <Link to={`/`} title="Get all users" className="intro__link">
                            <img src="client/assets/arrow_left.svg" alt="Go to all users"/>
                            <p className="intro__link-text">Back</p>
                        </Link>
                    </div>
                }
                <h2 className="heading-l">{props.message}</h2>
            </section>
        </div>
        
    )
}
export default Intro;