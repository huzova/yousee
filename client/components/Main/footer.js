import React from 'react';

const Footer = (props) => {
    return(
        <footer className="footer">
            <p className="footer__text">Have a nice day!</p>
        </footer>
    )
}

export default Footer;