import React from 'react';
import UsersListItem from './usersListItem';

const UsersList = (props) => {
    return (
        <section className="user-list">
            <div className="user-list__table-box">
                <div className="user-list__table-5col">
                    <header className="user-list__header">
                        <p className="user-list__row-label">name</p>
                        <p className="user-list__row-label">username</p>
                        <p className="user-list__row-label">email</p>
                        <p className="user-list__row-label">phone</p>
                        <p className="user-list__row-label">company name</p>
                    </header>
                    <div>
                        {props.list.map(users => (
                            <UsersListItem users={users} key={users.id}/>
                        ))}
                    </div>
                </div>
            </div>
        </section>
    )
}

export default UsersList;