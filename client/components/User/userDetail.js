import React from 'react';
import MapMarker from '../Main/mapMarker';

const UserDetail = props => {
    return (
        <section className="user-detail">
            <div className="table-box">
                <h3 className="heading-m table-box__title">Bacis info</h3>
                <div className="table-box__2col">
                    <div className="table-box__row">
                        <p className="table-box__label">Name</p>
                        <p className="table-box__value">{props.user.name}</p>
                    </div>
                    <div className="table-box__row">
                        <p className="table-box__label">User name</p>
                        <p className="table-box__value">{props.user.username}</p>
                    </div>
                    <div className="table-box__row">
                        <p className="table-box__label">Email</p>
                        <p className="table-box__value">{props.user.email}</p>
                    </div>
                    <div className="table-box__row">
                        <p className="table-box__label">Phone</p>
                        <p className="table-box__value">{props.user.phone}</p>
                    </div>
                    <div className="table-box__row">
                        <p className="table-box__label">Website</p>
                        <p className="table-box__value">{props.user.website}</p>
                    </div>
                </div>
            </div>

            <div className="table-box">
                <h3 className="heading-m table-box__title">Address</h3>
                <div className="table-box__2col">
                    <div className="table-box__row">
                        <p className="table-box__label">Street</p>
                        <p className="table-box__value">{props.user.address.street}</p>
                    </div>
                    <div className="table-box__row">
                        <p className="table-box__label">Suite</p>
                        <p className="table-box__value">{props.user.address.suite}</p>
                    </div>
                    <div className="table-box__row">
                        <p className="table-box__label">City</p>
                        <p className="table-box__value">{props.user.address.city}</p>
                    </div>
                    <div className="table-box__row">
                        <p className="table-box__label">Zipcode</p>
                        <p className="table-box__value">{props.user.address.zipcode}</p>
                    </div>
                    <div className="table-box__map">
                        <MapMarker
                            isMarkerShown
                            googleMapURL="https://maps.googleapis.com/maps/api/js?key=AIzaSyC74g3SOJbmOlId5T-0ZXsA9QdgHkgjV34&v=3.exp&libraries=geometry,drawing,places"
                            loadingElement={<div style={{ height: `100%` }} />}
                            containerElement={<div style={{ height: `400px` }} />}
                            mapElement={<div style={{ height: `100%` }} />}
                            lat={parseFloat(props.user.address.geo.lat)}
                            lng={parseFloat(props.user.address.geo.lng)}
                        />
                    </div>
                </div>
            </div>

            <div className="table-box">
                <h3 className="heading-m table-box__title">Company</h3>
                <div className="table-box__2col">
                    <div className="table-box__row">
                        <p className="table-box__label">Name</p>
                        <p className="table-box__value">{props.user.company.name}</p>
                    </div>
                    <div className="table-box__row">
                        <p className="table-box__label">catchPhrase</p>
                        <p className="table-box__value">{props.user.company.catchPhrase}</p>
                    </div>
                    <div className="table-box__row">
                        <p className="table-box__label">BS</p>
                        <p className="table-box__value">{props.user.company.bs}</p>
                    </div>
                </div>
            </div>

        </section>
    )
}

export default UserDetail;