import React from 'react';
import { Link } from 'react-router-dom';

const UsersListItem = ({users}) => (
        <Link to={`users/${users.id}`} title="Get user details" className="user-list__table-link">
            <div className="user-list__row">
                <div className="user-list__entry">
                    <p className="user-list__title--xs">{users.name}</p>
                </div>
                <div className="user-list__entry">
                    <p className="user-list__label--xs">username</p>
                    <p className="user-list__value">{users.username}</p>
                </div>
                <div className="user-list__entry">
                    <p className="user-list__label--xs">email</p>
                    <p className="user-list__value">{users.email}</p>
                </div>
                <div className="user-list__entry">
                    <p className="user-list__label--xs">phone</p>
                    <p className="user-list__value">{users.phone}</p>
                </div>
                <div className="user-list__entry">
                    <p className="user-list__label--xs">company name</p>
                    <p className="user-list__value">{users.company.name}</p>
                </div>
                <button className="user-list__button">
                    <img src="client/assets/arrow_right.svg" alt="Go to user details"/>
                </button>
            </div>
        </Link>
)

export default UsersListItem;