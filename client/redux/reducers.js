const userState = (state = {}, action) => {
    switch (action.type){
        case 'fetchUsers':
            return Object.assign({}, state, {
                users: action.data
            })
        case 'ErrFetchUsers':
            return Object.assign({}, state, {
                errFetchUsers: action.msg
            })
        case 'fetchUser':
            return Object.assign({}, state, {
                user: action.data
            })
        case 'ErrFetchUser':
            return Object.assign({}, state, {
                errFetchUser: action.msg
            })
    }
}

export default userState;