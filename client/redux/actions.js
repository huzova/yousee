const apiUrl = "http://localhost:3000";

export const fetchUsers = props => {
    return (dispatch) => {
        return fetch(`${apiUrl}/api/users`)
        .then((response) => response.json())
        .then(json => dispatch(
            {type: "fetchUsers", data: json}))
        .catch(err => dispatch(
            { type: "ErrFetchUsers", msg: "No users found" }))
    }
}

export const fetchUser = id => {
    return (dispatch, getState) => {
        const currentState = getState();
        if (currentState && currentState.users) {
            const user = currentState.users.find(user => user.id == id);
            if (user)
                return dispatch({type: "fetchUser", data: user})
        } 
        return fetch(`${apiUrl}/api/user/${id}`)
        .then((response) => response.json())
        .then(json => dispatch(
            {type: "fetchUser", data: json}))
        .catch(err => dispatch(
            { type: "ErrFetchUser", msg: "No users found" }))
    }
}