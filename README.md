# Simple React App
This repository contains a simple react app that lists some users in a table. After clicking on a user it is possible to see some additional information about that given user.

## Functionality
The application is powered by a simple backend written in node.js. Backend responses are delayed on purpose to emulate slow network or backend response.
The app contains two pages - [UserList](https://bitbucket.org/huzova/yousee/src/master/client/components/User/usersList.js) and [UserDetail](https://bitbucket.org/huzova/yousee/src/master/client/components/User/userDetail.js)
### Geolocation
UserDetail displays a Google Map with marker displaying user address coordinates.
### Redux
For managing state, the app uses Redux store. Whenever user details data are available in Redux store, they are not re-fetched from backend.

## Styling
### Block Element Modifier (BEM)
App utilizes SCSS and follows BEM naming convention (http://getbem.com/). 
### Responsive
The application is responsive with mobile-first approach. 
### Animations
I utilized CSS3 animations and transitions in multiple places.
* Custom loading indicator - I created 'loader-line-grow' animation which changes heigh of each of four loader lines, while animation of each line starts with a different delay.
* Back button - has a simple bounce animation triggered on hover over.

## Starting the application
* Clone repository to local folder (git clone https://huzova@bitbucket.org/huzova/yousee.git)
* Run `npm install` to install some dependencies
* Open terminal in project root directory
* Start the backend by running `node backend.js` 
* In another window, start the frontend by running `npm run-script frontend`
